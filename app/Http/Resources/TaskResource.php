<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
//use Carbon\Carbon;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $custom = json_decode($this->extendedProps);
        return [
            "id" => $this->id,
            "title" => $this->title ?? "",
            "start" => $this->start ?? "",
            "end" => $this->end ?? "",
            "color" => $this->color ?? "",
            "allDay" => $this->allDay ?? "",
            "className" => $this->className ?? "",
            "extendedProps" => [
                "tipo" => $custom->tipo,
                "monto" => $custom->monto,
                "nota" => $custom->nota,
                "color" => $this->color,
                "className" => $this->className ?? "",
                ]
            //'fecha' => Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y H:i:s'),

        ];
    }
}
