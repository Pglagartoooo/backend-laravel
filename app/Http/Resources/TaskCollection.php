<?php

namespace App\Http\Resources;
//use Carbon\Carbon;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($row, $key) {
                $custom = json_decode($row->extendedProps);
                return [
                    "id" => $row->id,
                    "title" => $row->title ?? "",
                    "start" => $row->start ?? "",
                    "end" => $row->end ?? "",
                    "color" => $row->color ?? "",
                    "allDay" => $row->allDay ?? "",
                    "className" => $row->className ?? "",
                    "extendedProps" => [
                        "tipo" => $custom->tipo,
                        "monto" => $custom->monto,
                        "nota" => $custom->nota,
                        "color" => $row->color,
                        "id" => $row->id,
                        "className" => $row->className ?? "",
                    ],
                    "ingresos"=>$custom->tipo=="INGRESO"? $custom->monto: 0,
                    "egresos"=>$custom->tipo=="EGRESO"? $custom->monto: 0,
                    "ahorro"=>$custom->tipo=="AHORRO"? $custom->monto: 0,
                    "descuentoAhorro"=>$custom->tipo=="DESCUENTO"? $custom->monto: 0,
                    //'fecha'=>Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d/m/Y H:i:s'),
                ];
            }),
            'links' => [
                'self' => 'link-value',
            ],
        ];
    }
}
