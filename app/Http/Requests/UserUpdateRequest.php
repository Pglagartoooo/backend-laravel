<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Validator;

class UserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = $this->input('id');
        return [

            'name' => 'nullable',
            'username' => 'required|alpha_num:ascii',Rule::unique('users')->ignore($id),
            'password'=>'nullable|min:4',


        ];
    }

    public function messages()
    {
        return [
            'username.required' => 'Este campo es obligatorio.',
            'password.required' => 'Este campo es obligatorio.',
        ];
    }


}
