<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;

class TaskStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {

        return [

            'title' => 'required',
            'start' => 'required',
            'end' => 'required',
            'color' => 'required',
            'allDay' => 'required',
            'className' => 'required',
            'extendedProps' => 'required',

            'extendedProps.className' => 'required',
            'extendedProps.tipo' => 'required',
            'extendedProps.monto' => 'required',
            'extendedProps.color' => 'required',
            'extendedProps.nota' => 'required',
            //'extendedProps.*.nota' => 'required',

        ];
    }

    public function messages()
    {
        return [

            'title.required' => 'Este campo es obligatorio.',
            'start.required' => 'Este campo es obligatorio.',
            'end.required' => 'Este campo es obligatorio.',
            'color.required' => 'Este campo es obligatorio.',
            'allDay.required' => 'Este campo es obligatorio.',
            'className.required' => 'Este campo es obligatorio.',
            'extendedProps.required' => 'Debe Seleccionar un producto',
            'extendedProps.className.required' => 'Este campo es obligatorio.',
            'extendedProps.tipo.required' => 'Este campo es obligatorio.',
            'extendedProps.monto.required' => 'Este campo es obligatorio.',
            'extendedProps.color.required' => 'Este campo es obligatorio.',
            'extendedProps.nota.required' => 'Este campo es obligatorio.',
            //'extendedProps.*.nota.required' => 'fila  # :position es obligatorio.',

        ];
    }

    public function after(): array
    {

        return [
            function (Validator $validator) {
                $errores = [];
                if ($validator->errors()->has('extendedProps.*')) {
                foreach ($validator->errors()->get('extendedProps.*') as $key => $message) {
                    // ...
                    array_push($errores, $message[0]);
                }
                $validator->errors()->add(
                    'campos',
                    $errores
                );
            }
            }
        ];
    }
}
