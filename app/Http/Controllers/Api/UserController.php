<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use Mail;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

    public function show($id)
    {
        $user = User::where('id', '=', $id)->first();
        return response()->json([
            'status' => true,
            'user' => $user,
        ], 200);
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::find($id);
        $user->name     = $request->name ?? '';
        $user->username     = $request->username;
        $user->lastname     = $request->lastname ?? '';
        $user->description     = $request->description ?? '';

        /*if($request->input('password')){
            $user->password = Hash::make($request->input('password'));
        }*/
        $user->save();

        return response()->json([
            'status' => true,
            'user' => $user,
        ], 200);
    }

    public function changePassword(Request $request, $id)
    {
        $user = User::find($id);
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return response()->json([
            'status' => true,
            'user' => $user,
        ], 200);
    }

    public function sendMailContact(Request $request)
    {
         //Indicamos que solo queremos recibir username, email y password de la request
         $data = $request->only('name', 'email', 'message');
         //Realizamos las validaciones
         $validator = Validator::make($data, [
             'name' => 'required|min:4',
             'email' => 'required|email',
             'message'  => 'required',
         ],[
            'required'=>'Este campo es obligatorio.',
            'min'=>'debe contener al menos :min caracteres.',
            'email'=>'debe ser una dirección de correo válida.'
         ]);
         //Devolvemos un error si fallan las validaciones
         if ($validator->fails()) {
             return response()->json(['errors' => $validator->messages()], 400);
         }
        $content = [
            'subject' => 'Email de contacto desde la Web',
            'body' => $request->message,
            'name' => $request->name,
            'email' => $request->email,
        ];

        Mail::to('senasrf9@gmail.com')->send(new ContactMail($content));

        return response()->json([
            'status' => true,
            'status' => "Mensaje Enviado",

        ], 200);
    }
}
