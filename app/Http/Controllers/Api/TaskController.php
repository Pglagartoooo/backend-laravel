<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\TaskStoreRequest;
use App\Models\Task;
use App\Http\Controllers\Controller;
use App\Http\Requests\TaskUpdateRequest;
use App\Http\Resources\TaskCollection;
use App\Http\Resources\TaskResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use JWTAuth;

use Exception;

class TaskController extends Controller
{

    protected $user;
    public function __construct(Request $request)
    {
        $token = $request->header('Authorization');
        if($token != '')
            //En caso de que requiera autentifiación la ruta obtenemos el usuario y lo almacenamos en una variable, nosotros no lo utilizaremos.
            $this->user = JWTAuth::parseToken()->authenticate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $user = auth()->user();
        $tasks=Task::where('user_id',$user->id)
        ->orderBy('start','DESC')
        ->get();

        $taskCollection=new TaskCollection($tasks);
        return response()->json([
            'status' => true,
            'tasks'=>$taskCollection,
        ], 200);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaskStoreRequest $request)
    {
        $user = auth()->user();

        DB::beginTransaction();
        try {

            //creando
            $task = Task::create([
                'title' => $request->title ?? '',
                'start' => $request->start ?? '',
                'end' => $request->end ?? '',
                'color' => $request->color ?? '',
                'allDay' => $request->allDay ?? '',
                'className' => $request->className ?? '',
                'extendedProps' => json_encode($request->extendedProps),
                'user_id' => $user->id,

            ]);


            DB::commit();
            return response()->json([
                'status' => true,
                'task'=>$task,
            ], 200);
        } catch (Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => false,
                'message'=>$e->getMessage(),
            ], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query= Task::findOrFail($id);
        $task= new TaskResource($query);
        return response()->json([
            'status' => true,
            'task'=>new TaskResource( $task),
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaskUpdateRequest $request, $id)
    {

        $dateStart=Carbon::createFromTimeString($request->start)->format('Y-m-d');
        $task = Task::find($id);
        $task->title = $request->title ?? '';
        $task->start = $dateStart ?? '';
        $task->end = $dateStart ?? '';
        $task->color = $request->color ?? '';
        $task->allDay = $request->allDay ?? '';
        $task->className = $request->className ?? '';
        $task->extendedProps = json_encode($request->extendedProps);
        $task->save();

        return response()->json([
            'status' => true,
            'task'=>new TaskResource( $task),
        ], 200);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $task = Task::find($id);
        $task->delete();
        return response()->json([
            'status' => true,
            'message' => 'Evento Eliminado',

        ], 200);
    }
}
