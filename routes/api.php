<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('auth')->group(function () {
    /*
    * Las rutas que se mencionan a continuación son públicas,
    * el usuario puede acceder a ellas sin ninguna restricción.
    */

    Route::post('/login', [AuthController::class, 'authenticate'])->name('authenticate');
    Route::post('/register', [AuthController::class, 'register'])->name('register');

    Route::post('/reset-password', [AuthController::class, 'sendPasswordResetLink'])->name('password');
    Route::post('/reset/password', [AuthController::class, 'callResetPassword'])->name('resetpassword');
});
Route::post('/sendmail', [UserController::class, 'sendMailContact'])->name('sendMailContact');
// Las rutas mencionadas a continuación están disponibles solo para los usuarios autenticados.
Route::group(['middleware' => ['jwt.verify']], function () {
    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
    // info usuario
    Route::post('get-user', [AuthController::class, 'getUser']);
    Route::post('/user/update/{id}', [UserController::class, 'update'])->name('users.update');
    Route::post('/user/password/{id}', [UserController::class, 'changePassword'])->name('users.password');
    Route::get('/user/{id}', [UserController::class, 'show'])->name('users.show');

    //tasks
    Route::post('/task/update/{id}', [TaskController::class, 'update'])->name('tasks.update');
    Route::get('/task/{id}', [TaskController::class, 'show'])->name('tasks.show');
    Route::get('/task', [TaskController::class, 'index'])->name('tasks.index');
    Route::post('/task', [TaskController::class, 'store'])->name('tasks.store');
    Route::delete('/task/{id}', [TaskController::class, 'destroy'])->name('tasks.destroy');
});
