FROM richarvey/nginx-php-fpm:1.9.1

COPY . .

# Image config
ENV SKIP_COMPOSER 1
ENV WEBROOT /var/www/html/public
ENV PHP_ERRORS_STDERR 1
ENV RUN_SCRIPTS 1
ENV REAL_IP_HEADER 1

# Laravel config
ENV APP_ENV production
ENV APP_DEBUG false
ENV LOG_CHANNEL stderr

# Allow composer to run as root
ENV COMPOSER_ALLOW_SUPERUSER 1

ENV DATABASE_URL postgres://admin:RWeu3IIomt3UmG9OpxDKWaW05mnuJPvm@dpg-cmnjaggcmk4c738klou0-a/dbsrf_1amt
ENV DB_CONNECTION pgsql
ENV APP_KEY base64:xvKFec9fEnwE2kt7xdpgOedb8rXdIi2Ht01bx3ZktiE=
ENV JWT_SECRET ED9MxzAnA0GMnx3LopMVVck9TK8u1jX9UQdEyWf7Fcxoev1vIYxGStEfCWOHL0up
ENV JWT_TTL null

CMD ["/start.sh"]
